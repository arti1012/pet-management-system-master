package com.pet.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="mypets")
public class MyCart {

	    private String username;
	    private String pet;
	    @Id
	    private String petname;
	    private String petbreed;
	    private String lifespan;
	    private String color;
	
		
		public MyCart(String pet, String petname, String petbreed, String lifespan, String color) {
			super();
			this.pet = pet;
			this.petname = petname;
			this.petbreed = petbreed;
			this.lifespan = lifespan;
			this.color = color;
			
		}
		public MyCart() {
			// TODO Auto-generated constructor stub
		}
		
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPet() {
			return pet;
		}
		public void setPet(String pet) {
			this.pet = pet;
		}
		public String getPetname() {
			return petname;
		}
		public void setPetname(String petname) {
			this.petname = petname;
		}
		public String getPetbreed() {
			return petbreed;
		}
		public void setPetbreed(String petbreed) {
			this.petbreed = petbreed;
		}
		public String getLifespan() {
			return lifespan;
		}
		public void setLifespan(String lifespan) {
			this.lifespan = lifespan;
		}
		public String getColor() {
			return color;
		}
		public void setColor(String color) {
			this.color = color;
		}
		
}
