package com.pet.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.pet.model.MyCart;
import com.pet.model.PetDetails;
import com.pet.model.User;

public class DaoUserImplementation implements DaoUser {

	@Override
	public void userLogin(User user) {
		Session session = new Configuration().configure("hibernate-cfg.xml").buildSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(user);
		tx.commit();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean checkLogin(String userName, String password) {
		Session session = new Configuration().configure("hibernate-cfg.xml").buildSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		boolean isValidUser = false;
		Query query = session.createQuery("from User  where username=?1 and password=?2");
		query.setParameter(1, userName);
		query.setParameter(2, password);
		List list = query.list();
		if ((list != null) && (list.size() > 0)) {
			isValidUser = true;
		}
		tx.commit();
		return isValidUser;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PetDetails> viewpets() {
		Session session = new Configuration().configure("hibernate-cfg.xml").buildSessionFactory().openSession();
		session.beginTransaction();
		List<PetDetails> list = new ArrayList<PetDetails>();
		@SuppressWarnings("rawtypes")
		Query q = session.createQuery("from PetDetails");
		List<PetDetails> li = q.list();
		for (PetDetails petDetails : li) {
			list.add(petDetails);
		}

		return list;
	}

	@Override
	public void addpets(PetDetails petdetails) {
		Session session = new Configuration().configure("hibernate-cfg.xml").buildSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(petdetails);
		tx.commit();

	}

	@Override
	public List<PetDetails> getByPetName(PetDetails petdetails) {
		Session session = new Configuration().configure("hibernate-cfg.xml").buildSessionFactory().openSession();
		session.beginTransaction();
		List<PetDetails> list = new ArrayList<PetDetails>();
		PetDetails pet = new PetDetails();
		String q = "from PetDetails where petname=?1";
		if (!q.isEmpty()) {
			@SuppressWarnings("rawtypes")
			Query query = session.createQuery(q);
			query.setParameter(1, pet.getPetname());
			for (PetDetails petdetails1 : list) {
				pet.getPetname();
				pet.setPet(petdetails1.getPet());
				pet.setPetbreed(petdetails1.getPetbreed());
				pet.setLifespan(petdetails1.getLifespan());
				pet.setPopularity(petdetails1.getPopularity());
				pet.setColor(petdetails1.getColor());
				pet.setCost(petdetails1.getCost());

			}

		}
		return list;
	}

	@Override
	public void insertIntoMyPets(MyCart mypets) 
	{
		Session session = new Configuration().configure("hibernate-cfg.xml").buildSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(mypets);
		tx.commit();
	}

	@Override
	public List<MyCart> viewmypets() {
		Session session = new Configuration().configure("hibernate-cfg.xml").buildSessionFactory().openSession();
		session.beginTransaction();
		List<MyCart> list = new ArrayList<MyCart>();
		@SuppressWarnings("rawtypes")
		Query q = session.createQuery("from MyCart ");
		List<MyCart> li = q.list();
		for (MyCart mycart : li) {
			list.add(mycart);
		}

		return list;
	}
}