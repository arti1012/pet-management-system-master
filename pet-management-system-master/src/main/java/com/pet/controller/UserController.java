package com.pet.controller;

import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.pet.dao.DaoUser;
import com.pet.dao.DaoUserImplementation;
import com.pet.model.MyCart;
import com.pet.model.PetDetails;
import com.pet.model.User;

@Controller
public class UserController {
	DaoUser dao = new DaoUserImplementation();

	@RequestMapping("register")
	public ModelAndView insert() {
		return new ModelAndView("register", "user", new User());

	}

	@RequestMapping("adduser")
	public ModelAndView adduser(@ModelAttribute("user") User user) {
		dao.userLogin(user);
		return new ModelAndView("display", "user", user);
	}

	@RequestMapping("loginuser")
	public ModelAndView login() {
		return new ModelAndView("loginuser", "userlogin", new User());
	}

	@RequestMapping("validate")
	public ModelAndView validateuser(@RequestParam("userName") String userName,
			@RequestParam("password") String password, @ModelAttribute("userlogin") User user) {
		DaoUserImplementation daouser = new DaoUserImplementation();
		boolean isValidUser = daouser.checkLogin(userName, password);

		if (isValidUser) {
			return new ModelAndView("succespage", "loginsuccess", "Login Success!");
		} else {

			return new ModelAndView("loginuser", "loginsuccess", "Login Fail!");
		}

	}

	@RequestMapping("viewpets")
	public ModelAndView view() {
		List<PetDetails> list = dao.viewpets();
	
		return new ModelAndView("viewpetdetails", "view", list);

	}

	@RequestMapping("addpetsdetails")
	public ModelAndView insertpets() {
		return new ModelAndView("addpetsdetails", "pets", new PetDetails());

	}

	@RequestMapping("addpets")
	public ModelAndView addupets(@ModelAttribute("pets") PetDetails petdetails) {
		dao.addpets(petdetails);
		return new ModelAndView("display", "pets", petdetails);
	}

	@RequestMapping("viewbuypets")
	public ModelAndView viewBuyPets(@RequestParam("pet") String pet, @RequestParam("petname") String petname,
			@RequestParam("petbreed") String petbreed, @RequestParam("lifespan") String lifespan,
			@RequestParam("color") String color, @RequestParam("cost") double cost, PetDetails petdetails) {
		List<PetDetails> list = dao.getByPetName(petdetails);
		list.add(petdetails);

		return new ModelAndView("getpetdetails", "view", list);

	}

	@RequestMapping("addtomypets")
	public ModelAndView mypets(@ModelAttribute("userpets") MyCart mypets) {

		dao.insertIntoMyPets(mypets);
		return new ModelAndView("getmypetspage", "userpets", mypets);
	}

	@RequestMapping("mypet")
	public ModelAndView addpets(@ModelAttribute("userpets") MyCart mypets) {
		
		return new ModelAndView("mypetsbyuser", "userpets", new MyCart());

	}
	@RequestMapping("mycart")
	public ModelAndView mycart()
	{
		List<MyCart> list=dao.viewmypets();
		return new ModelAndView("display", "viewmypets", list);
	}
	
   @RequestMapping("logout")
   public ModelAndView logout()
   {
	   return new ModelAndView("logout");
   }
   
}
