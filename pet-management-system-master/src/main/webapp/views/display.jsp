<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>MyCart</title>
<style>
body {
	background-color: #49b7ac;
}

h1 {
	color: white;
	text-align: center;
}

p {
	font-family: verdana;
	font-size: 20px;
}

a {
	background-color: black;
	color: white;
	padding: 1em 1.5em;
	text-decoration: none;
}
</style>
</head>
<body>
<h1>My Cart</h1>
<hr/>
	<table align="center" border="10">
		<tr>
			<th>User Name</th>
			<th>Pet Name</th>
			<th>Pet Breed</th>
			<th>Life Span</th>
			<th>Color</th>
		</tr>
		<c:forEach items="${viewmypets}" var="mycart">
			<tr>
				<td>${mycart.getUsername()}</td>
				<td>${mycart.getPetname()}</td>
				<td>${mycart.getPetbreed()}</td>
				<td>${mycart.getLifespan()}</td>
				<td>${mycart.getColor()}</td>
			</tr>
		</c:forEach>
	</table>


</body>
</html>